# Basis docker project

## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [About](#about)
- [Part 3](#part-3)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Requirements

Requires `docker`.

## Setup

    $ docker build . -t rainbow
    $ docker run --rm -v $(pwd):/usr/src/ -w /usr/src/ rainbow


## About
This project shows how to use CMake inside a docker container.

It compiles my solution to the rainbow kata and tests it. 

## Part 3
A random use case of docker/minecraft to manage your containers.
https://github.com/docker/dockercraft

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## Contributers and License
Copyright 2022,

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

