#include "functions.h"
std::random_device randomDevice;     
std::mt19937 randomEngine(randomDevice());    
std::uniform_int_distribution<int> uniformDistribution(17,230); 

// \033[38;5;<N>m   ,   <N> = 17 - 231
// https://en.wikipedia.org/wiki/ANSI_escape_code
std::string add_color(const int &color)
{
    std::string text = "\x1B[38;5;"+std::to_string(color)+"m";
    return text;
}

void print_color(const std::string &text)
{
    int color;
    int size = text.size();
    for(int j=0; j<size;j++)
    {
        color = uniformDistribution(randomEngine);
        std::cout<<add_color(color)<<text[j];
    }
    std::cout<<" "; 
}
