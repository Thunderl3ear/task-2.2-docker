// Author: Thorbjoern
#include "functions.h"

int main(int argc, char* argv[])
{
    std::string text;
    if(argc>1)
    {
        for(int i=1; i<argc; i++)
        {
            text = argv[i];
            print_color(text);
        }
        std::cout<<"\033[0m"<<std::endl;
    }
    return 0;
}
